module Data.Graphviz (digraph) where

import Frelude
import qualified Data.Text as Text

digraph :: [NonEmpty Text] -> Text
digraph es = Text.unlines
  [ "digraph _ {"
  , edges es
  , "}"
  ]

edges :: [NonEmpty Text] -> Text
edges = Text.unlines . fmap f
  where
  f (x :| xs) = Text.unlines $ (\y -> x <> " -> " <> y <> ";") <$> xs
