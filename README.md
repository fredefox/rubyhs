# rubyhs

A *node* is the "fully qualified name" of either 1) a function; or 2)
a module.  The program should create a *reference graph*.  The
reference graph shows which nodes reference other nodes.  Treating
modules and functions equally allow us to express that a module calls
a function as in:

    module M
      f
    end

as well as the opposite where a function refers to a constant:

    def f
      M
    end

The program should ensure that references actually correspond to nodes
that we know of.

The program should maintain forward edges as well as backwards edges.
That way we can both answer the question "Which nodes does this node
transitively reference" as well as "which nodes transitively reference
this node".

Given a reference graph and a node - called the query - that program
should print out a spanning tree rooted at the query node.  Cycles
should be marked.  E.g. like so for two mutually defined nodes `a` and
`b`:

    {
      "a": {
        "b": { "a": "__cycle__" }
      }
    }
