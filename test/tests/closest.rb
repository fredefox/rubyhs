module A
  module B0
    def self.b0
      :b0
    end
  end
  module B1
    def self.b1
      B0.b0
    end
  end
end

puts(A::B1.b1)
