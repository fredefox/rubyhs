module A
  module B
    def self.a
      :shallow
    end

    puts(A::B.a)

    module A
      module B
        def self.a
          :deep
        end
      end
    end

    # Had The inner A::B not been declared this would refer to the one
    # that returns :shallow.
    puts(A::B.a)
  end
end
