{-# LANGUAGE BangPatterns #-}
{-# OPTIONS_GHC -Wall #-}
module Main (main) where

import Frelude
import Test.Tasty
import Test.Tasty.HUnit
import qualified Data.Language.Ruby as Ruby
import qualified Rubyhs.References as Ruby
import qualified Data.Aeson as Aeson
import System.FilePath
import Data.HashMap.Strict (HashMap)
import Data.Set (Set)
import System.Directory (withCurrentDirectory)

main :: IO ()
main = withCurrentDirectory "test" $ defaultMain $ testGroup "Unit tests" tests

tests :: [TestTree]
tests = go <$> ["mod", "simple", "nested", "closest", "assignment"]
  where
  go :: String -> TestTree
  go s = testCase s $ do
    let basename = "tests" </> s
    begin <- Ruby.parseFile @Ruby.Begin $ basename <.> "rb"
    json  <- decodeFile @(HashMap Ruby.Node (Set Ruby.Node)) $ basename <.> "json"
    let (Ruby.Result refs) = Ruby.references begin
    when (refs /= json)
      $ assertFailure
      $ "Expected " <> convertString (Aeson.encode json) <> " but got " <> convertString (Aeson.encode refs)

decodeFile :: FromJSON a => FilePath -> IO a
decodeFile p = Aeson.eitherDecodeFileStrict' p >>= \case
  Left err -> error err
  Right a -> pure a
