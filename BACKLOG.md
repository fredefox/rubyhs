There still an issue with inferring for a given function call /
constant reference where that thing is defined.

We're currently generating the forest of references from the call
graph by calculating a spanning forest.  There's a problem with that.
Consider the following map of call-sites:

    {
      a: [b]
      b: []
    }

The following is a spanning tree:

    {
      a: []
      b: []
    }

Here it looks like `a` doesn't call `b`.


We should also store all the call-sites.  These will end up as leaves
in the call tree.


    {
      "K": {},
      "A": {},
      "f": {
        "f": "__cycle__"
      },
      "A.f": {
        "K.g": {}
      }
    }


The above problem is sort of fixed by just writing DOT output as it
takes care of cycles.

# Instance methods
Instance methods are a common occurrence in Ruby code.  However they
aren't really resolved in a nice way.  E.g.:

    def f
      person.name
    end

Will produce:

    {"f":["name"]}

Which is misleading.  What we could do it try and find out what
`person` is referring to and then "inline" it.  For example.  Assume
that we have `"person": ["Person.new"]` in our references then we
could add an entry for `f` like so: `"f": ["Person.name"]`.
