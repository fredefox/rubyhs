def h
end

module M
  def f
  end

  def g
    f(2)
    h(2, a: 'a')
    h(2, a: :a)
  end

  def h
  end

  module N
    def h
    end

    def f
      M::foo
      h
    end

    module A::B
      def k
      end
    end

    def g
    end
  end
end

module K
  f
end

f(2)
h(2, a: 'a')
h(2, a: :a)
